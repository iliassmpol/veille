PRISM (également appelé US-984XN1), est un programme américain de surveillance électronique par la collecte de renseignements à partir d'Internet et d'autres fournisseurs de services électroniques2,3,4,note 1. Ce programme classé, relevant de la National Security Agency (NSA), prévoit le ciblage de personnes vivant hors des États-Unis10. PRISM est supervisé par la United States Foreign Intelligence Surveillance Court (FISC) conformément au FISA Amendments Act of 2008 (FISA)11.

Edward Snowden, ex-consultant de la NSA, a dénoncé ce programme ; les périodiques The Guardian et The Washington Post ont signalé son existence le 6 juin 2013.

PRISM est utilisé en conjonction avec le programme Upstream par les autorités de sécurité américaines.

Edward Snowden, ex-consultant de la NSA, a dénoncé ce programme ; les périodiques The Guardian et The Washington Post ont signalé son existence le 6 juin 2013.

PRISM est utilisé en conjonction avec le programme Upstream par les autorités de sécurité américaines.



Testing revert commit
